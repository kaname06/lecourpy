from django.db import models
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser, PermissionsMixin

class UserManager(BaseUserManager):
    def _create_user(self, name, last_name, email, password, is_admin, is_superuser, **extra_fields):
        user = self.model(
            email = email,
            name = name,
            last_name = last_name,
            is_admin = is_admin,
            is_superuser = is_superuser,
            **extra_fields
        )
        user.set_password(password)
        user.save(using=self.db)
        return user

    def create_user(self, name, last_name, email, password=None, **extra_fields):
        return self._create_user(name, last_name, email, password, False, False, **extra_fields)

    def create_superuser(self, name, last_name, email, password=None, **extra_fields):
        return self._create_user(name, last_name, email, password, True, True, **extra_fields)

class User(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField('Correo Electrónico',max_length = 255, unique = True,)
    name = models.CharField('Nombres', max_length = 255, blank = True, null = True)
    last_name = models.CharField('Apellidos', max_length = 255, blank = True, null = True)
    phone = models.CharField('Teléfono', max_length=20)
    is_active = models.BooleanField(default = True)
    is_admin = models.BooleanField(default = False)
    token = models.CharField('Token', max_length=255, blank=True, null=True)
    
    objects = UserManager()

    class Meta:
        verbose_name = 'Usuario'
        verbose_name_plural = 'Usuarios'

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['name','last_name']

    def __str__(self):
        return f'{self.name} {self.last_name}'

class Teacher(models.Model):
    user = models.ForeignKey(User, on_delete=models.PROTECT)
    profession = models.CharField('Profesión', max_length=50)
    #include here classes relationship

    class Meta:
        verbose_name = 'Profesor'
        verbose_name_plural = 'Profesores'

    def __str__(self):
        return f'Profesor {self.user.name}'

class Student(models.Model):
    user = models.ForeignKey(User, on_delete=models.PROTECT)

    class Meta:
        verbose_name = 'Estudiante',
        verbose_name_plural = 'Estudiantes'

    def __str__(self):
        return f'Estudiante {self.user.name}'