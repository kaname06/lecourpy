from django.contrib import admin

from .models import Course, Class, Module, Rating, Comments

admin.site.register(Course)
admin.site.register(Class)
admin.site.register(Module)
admin.site.register(Rating)
admin.site.register(Comments)
