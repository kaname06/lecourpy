from django.db import models

from apps.users.models import Teacher, Student

class Category(models.Model):
    name = models.CharField('Name', max_length=255)

    class Meta:
        verbose_name = 'Categoria Curso'
        verbose_name_plural = 'Categorias Curso'

    def __str__(self):
        return self.name

class Course(models.Model):
    name = models.CharField('Nombre', max_length=255)
    short_description = models.CharField('Descripción corta', max_length=255, default="")
    description = models.TextField('Descripción')
    icon = models.URLField('Icono', max_length=300)
    is_active = models.BooleanField('Activo', default=True)
    categories = models.ManyToManyField(Category, related_name='course_category', blank=False)
    teacher = models.ForeignKey(Teacher, on_delete=models.PROTECT, related_name='course_teacher', verbose_name='Profesor', default=None)
    students = models.ManyToManyField(Student, through='Enrollment')

    class Meta:
        verbose_name = 'Curso'
        verbose_name_plural = 'Cursos'

    def __str__(self):
        return self.name

class Enrollment(models.Model):
    course = models.ForeignKey(Course, on_delete=models.PROTECT)
    student = models.ForeignKey(Student, on_delete=models.PROTECT)
    created_at = models.DateTimeField('Fecha de Inscripción', auto_now=False, auto_now_add=True)
    is_active = models.BooleanField('Activo', default=True)
    is_approved = models.BooleanField('Aprobado', default=False)
    signature = models.CharField('Firma de aprobación', max_length=255, blank=True)
    approved_date = models.DateField('Fecha de aprobación', auto_now=False, auto_now_add=False, blank=True)

    class Meta:
        unique_together = [['course', 'student']]

    def __str__(self):
        return f'Registro de {self.student.user.name} en {self.course.name}'

class Module(models.Model):
    name = models.CharField('Nombre', max_length=255)
    course = models.ForeignKey(Course, on_delete=models.PROTECT, related_name='module_course', verbose_name='Curso')

    class Meta:
        verbose_name = 'Modulo'
        verbose_name_plural = 'Modulos'

    def __str__(self):
        return self.name

class Class(models.Model):
    title = models.CharField('Titulo', max_length=255)
    uri = models.URLField('Url del video', max_length=255)
    is_active = models.BooleanField('Activa', default=True)
    is_public = models.BooleanField('Publica', default=False)
    module = models.ForeignKey(Module, on_delete=models.PROTECT, related_name='class_module', verbose_name='Modulo')
    student_views = models.ManyToManyField(Student, related_name='classes_viewed')

    class Meta:
        verbose_name = 'Clase'
        verbose_name_plural = 'Clases'

    def __str__(self):
        return self.title

class Comments(models.Model):
    class_ref = models.ForeignKey(Class, on_delete=models.PROTECT, related_name='class_comments', verbose_name='Clase')
    comment = models.TextField('Comentario')
    created_at = models.DateTimeField('Creación', auto_now=False, auto_now_add=True)

    class Meta:
        verbose_name = 'Comentario de clase'
        verbose_name_plural = 'Comentarios de clase'

    def __str__(self):
        return self.comment

class Rating(models.Model):
    course = models.ForeignKey(Course, on_delete=models.PROTECT, related_name='rating_course', verbose_name='Curso')
    rating = models.PositiveIntegerField('Calificación')
    comment = models.TextField('Comentario')

    class Meta:
        verbose_name = 'Calificación de curso'
        verbose_name_plural = 'Calificaciones de curso'

    def __str__(self):
        return f'{self.id} - curso: {self.course.name}'

class Exam(models.Model):
    course = models.ForeignKey(Course, on_delete=models.PROTECT, related_name='exam_course')

    class Meta:
        verbose_name = 'Examen'
        verbose_name_plural = 'Examanes'

    def __str__(self):
        return f'Exam from course {self.course.name}'
    
class Question(models.Model):
    exam = models.ForeignKey(Exam, on_delete=models.CASCADE)
    content = models.TextField('Enunciado')

class Answer(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    content = models.TextField('Respuesta')
    is_correct = models.BooleanField('Correcta', default=False)

class ViewedClasses(models.Model):
    student = models.ForeignKey(Student, on_delete=models.PROTECT, related_name='viewed_classes', related_query_name='viewed')
    class_ref = models.ForeignKey(Class, on_delete=models.CASCADE)
    viewed_at = models.DateTimeField('Fecha de registro', auto_now=False, auto_now_add=True)

    class Meta:
        verbose_name = 'Clase vista'
        verbose_name_plural = 'Clases vistas'