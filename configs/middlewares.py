from django.core.exceptions import ObjectDoesNotExist
from rest_framework.status import *
from rest_framework.response import Response
from jwt import DecodeError

from configs.utils import jwt_decode_handler
from apps.users.models import User

ADMIN = 'admin'
LOGGED_USER = 'user'

class Authorize(object):

    def __init__(self, get_response, protecteddMethods = [], role = None):
        self.model = User
        self.role = role
        self.protecteddMethods = protecteddMethods

    def process_request(self, request):
        if request.method in self.protecteddMethods or len(self.protecteddMethods) == 0:
            try:
                token = request.META['HTTP_SSID']
            except KeyError:
                return Response({'errors': ['No se ha suministrado el token de AUTHORIZATION']}, status=HTTP_401_UNAUTHORIZED)
            
            try:
                data = jwt_decode_handler(token)
            except DecodeError:
                return Response({'errors': ['Token no valido.']}, status=HTTP_401_UNAUTHORIZED)

            try:
                user = self.model.objects.get(id=data['id'], token=token)
                request.user = user
                if self.role is not None:
                    if self.role == 'admin':
                        if user.is_admin:
                            request.isAdmin = True
                        else:
                            return Response({'errors': ['Permiso de acceso denegado a esta ruta']}, status=HTTP_401_UNAUTHORIZED)
                    elif self.role == 'user' and not user.is_admin:
                        request.isAdmin = False
                    else:
                        return Response({'errors': ['Usuario no autorizado.']}, status=HTTP_401_UNAUTHORIZED)
            except ObjectDoesNotExist:
                return Response({'errors': ['Usuario no autorizado.']}, status=HTTP_401_UNAUTHORIZED)

class AuthorizeAdmin(Authorize):
    def __init__(self, get_response, protecteddMethods = []):
        self.model = User
        self.role = 'admin'
        self.protecteddMethods = protecteddMethods

class AuthorizeUser(Authorize):
    def __init__(self, get_response, protecteddMethods = []):
        self.model = User
        self.role = 'user'
        self.protecteddMethods = protecteddMethods