import jwt
from configs.settings import base as settings
from datetime import datetime


def jwt_encode_handler(payload):
    payload['time_generate'] = datetime.now().timestamp()
    return jwt.encode(payload, settings.SECRET_KEY, algorithm='HS256')

def jwt_decode_handler(token):
    return jwt.decode(token, settings.SECRET_KEY, algorithms=['HS256'])
